#!/bin/bash
echo ""
echo "gitlab"
echo "Maintainer: Benoit Lavorata"
echo "License: MIT"
echo "==="
echo ""

FILE=".env"
if [ -f $FILE ]; then
    echo ""
    echo "File $FILE exists, will now start: ..."
    export $(cat .env | xargs)

    echo ""
    echo "Create networks ..."
    docker network create $GITLAB_CONTAINER_NETWORK

    echo ""
    echo "Create volumes ..."
    docker volume create --name ${GITLAB_CONTAINER_NAME}_${GITLAB_CONTAINER_VOLUME_DATA}
    docker volume create --name ${GITLAB_CONTAINER_NAME}_${GITLAB_CONTAINER_VOLUME_CONFIG}
    docker volume create --name ${GITLAB_CONTAINER_NAME}_${GITLAB_CONTAINER_VOLUME_LOGS}
    
    FILE_TEMPLATES=".templates_copied"
    echo ""
    if [ -f $FILE_TEMPLATES ]; then
        echo "Templates already deployed ..."
    else
        echo "Copy template files ..."
        touch $FILE_TEMPLATES
    fi

    echo ""
    echo "Boot ..."
    docker-compose up -d --remove-orphans $1
else
    echo "File $FILE does not exist: deploying for first time"
    cp .env.template .env
    
    echo -e "Make sure to modify .env according to your needs, then use ./up.sh again."
fi

