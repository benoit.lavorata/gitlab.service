#!/bin/bash
docker volume create gitlab_config
docker volume create gitlab_data
docker volume create gitlab_logs
docker-compose up